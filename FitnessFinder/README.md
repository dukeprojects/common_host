> Please note: the original app description is at: https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder. Since the app is undisclosed under *Duke's Center for Mobile Development*, you need to login to Duke GitLab service before accessing its dependencies.

---

<div align="center">
   <img width="256" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/logo.png" alt="FitnessFinder Logo">
</div>

<div align="center">
    ![Swift](https://img.shields.io/badge/Swift-5.1-orange.svg?style=flat)
    ![Xcode](https://img.shields.io/badge/Xcode-11.0-blue.svg?style=flat)
    ![macOS](https://img.shields.io/badge/macOS-10.14.5-green.svg?style=flat)
    ![version](https://img.shields.io/badge/version-1.0.4%20build%207-blue.svg?style=flat)
    ![Django](https://img.shields.io/badge/django-2.1-blue.svg?style=flat)
    ![Research Kit](https://img.shields.io/badge/Research%20Kit-2.0-brightgreen.svg)
</div>

<div align="center">
    <h1>FitnessFinder app - your pathway towards a healthier lifestyle.</h1>
</div>

[FitnessFinder](https://appstore.colab.duke.edu/apps/77) helps you to find the path toward a healthier life. It is a counseling & feedback app for physicians to help people to find various activity choices near their home. The app aims to provide a easy-to-use user experience to quickly find out appropriate activities for people at different age, with different tastes and have miscellaneous needs. Furthermore, the app is designed to make the life easier for both the end users and the database maintainers with fullly graphical interactions.

This is the first app that Ting delivered as a developer in *Duke's Center for Mobile Development*. Me and [Yuanyuan Yu](https://www.linkedin.com/in/yuanyuan-yu-706168152/) collaborated to build the app from scratch, and finally released the product, which is being put into daily use and with more than 30 installations from *Duke School of Medicine*.

## Idea and Progress notes

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/idea.jpg" title="Idea" width="320">
</div>

Please refer to the [notes](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/tree/master/progress-notes/progress-notes.md) and the [folder](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/tree/master/progress-notes) to learn more about the evolution of this app.

The idea comes from our clients of *Duke School of Medicine*. Initially they wanted to have an app that can search among various activities for local citizens. The first idea validation prototype was designed according to the sketches and similar products provided by them. 

After the first sprint of development, we had more in-depth discussion with them and refactored the UI to current style. During the [second](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/-/milestones/2) (Deal with data persistence and table view demonstration) and the [third](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/-/milestones/3) (Query, logistic discussion with customer, UI optimization) sprint, more details including database design, logic design and user experience improvements are done.

In the fourth sprint right now, our clients are testing with the app in their department, gathering feedbacks from physicians and ready to put the release version into daily use in Fall 2019.

## Features

FitnessFinder app has many features and highlights, including but not limited to:

- [x] Questionnaire and filtering with `ResearchKit` by Apple
- [x] Back-end activity database management with `Django` Admin and client side ORM with `fmdb`
- [x] Geo-location based result filtering with `CoreLocation` framework
- [x] Activity detail exhibition with `Carbon` and `Lightbox`
- [x] Activity information sharing in rich-text format
- [x] Activity information sharing with long screenshot
- [x] Result demonstration with `Charts`
- [x] Image fetching and caching with `SDWebImage`
- [x] Reachability detection and offline handling
- [x] ...And many more bonus perks yet to be discovered in the app! ;-) 😉

## Example

<div align="center">
    <img src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/random1.gif" alt="Pick a random activity">
</div>

Above is a simple sample of "Pick a random activity" feature in FitnessFinder. Please check the demo videos below for the basic usage and features of the app!

## Usage

**Please note:** videos on GitLab seems not to be able to play on mobile devices.  
Please visit the page with desktop browser to have a better experience.

| `Walk-through` | `Show all` | `app info page` |
|:---------------:|:---------------:|:---------------:|
|![Walk-through](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/walkthrough.mov)|![Show all](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/showall.mov)|![Info](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/infopage.mov)|
|This is a general walk-through of the basic workflow of FitnessFinder app. The physician will first ask a few general questions, including participant's age, location, and distance he/she wills to travel. Then a list of activities that meets the requirements will show, and the user can thus use filters to more accurately find what they want. The basic information of the activity will be provided, and user can visit its website to see the details.|A show all button is provided for general cases, when the physician want to check all available activities, or for database maintainer to validate their input. It will show all the valid activities in North Carolina without flags and filters.|The info page gives the app version, author name and an entrance for feedback.|

| `Offline` | `Filter` | `Map` |
|:---------------:|:---------------:|:---------------:|
|![Offline](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/offline.mov)|![Filter](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/filter.mov)|![Map](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/videos/map.mov)|
|When the device is offline, FitnessFinder app will fallback to the offline mode, which loads data from latest updated local storage. It will also try to load the cached geo-location data and, if cache misses, it will temporarily hide that activity.|An extra filter page is embedded in the result table. Users can make use of it to precisely locate their favorite activities.|Users can also use a map to see what are the activities surrounding their location, and tap into the details to learn more about them.|

Videos are compressed with `FFmpeg`.
```bash
ffmpeg -i <input>.mov -vf scale=640:-1 <output>.mov
```

## Designs and highlights

| `ResearchKit` | `Eureka` | `Carbon` | `Charts` |
|:---------------:|:---------------:|:---------------:|:---------------:|
|![ResearchKit](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/RK.png)|![Eureka](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/Eureka.png)|![Carbon](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/Carbon.png)|![Charts](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/Charts.png)|

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/RK2.png" title="ResearchKit" width="320">
</div>

### ResearchKit, Eureka, Carbon and more - Look and feel, UI/UX

I introduced multiple 3rd party frameworks to give this app a unified, clean and steady look, as I'm not an expert in UI design, and I also want to focus on the architecture as well as the logics.

At first I used native UIKit widgets to create a idea validation prototype. While that app works and demonstrate well the whole logic, it is not nice looking, nor easy to use. Therefore, in the second sprint of development, I first started with the goal: find a good-looking, questionnaire-like framework to use. Luckily, after some research, I found [ResearchKit](http://researchkit.org/index.html) powered by Apple. 

> ResearchKit is an open source framework introduced by Apple that allows researchers and developers to create powerful apps for medical research. Easily create visual consent flows, real-time dynamic active tasks, and surveys using a variety of customizable modules that you can build upon and share with the community. And since ResearchKit works seamlessly with HealthKit, researchers can access even more relevant data for their studies — like daily step counts, calorie use, and heart rate.

As the development got deeper, I added Eureka and Carbon for content display. Both frameworks are form generator, which enabled me to layout content in reusable cells with great ease, without worrying about constraints, alignment or scrolling issues.

#### Participation in open source

<div>
    <img style="float: left" align="left" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/rklogo.png" title="ResearchKit logo" width="160">
</div>

[PR#1256](https://github.com/ResearchKit/ResearchKit/pull/1256)

As I gradually dived deeper into ResearchKit framework, I confronted several limitations in their design. Also, I spotted a few bugs in the framework and opened multiple issues - #1255, #1269 and #1270 to discuss with the maintainers, as I was testing the framework with various screen dimensions.

Whereas I have learned iOS programming for a while in March, I'm not at all familiar with Objective-C code, and could hardly deal with its syntax. Therefore, I started to read some really basic introduction to it, and tried to understand the logic behind the scenes. Then I tested the bug fix with all devices before making the pull request.

#### Personal implementation

issue #32

As I need to make some internal changes to the original framework, I made my [own fork](https://github.com/yo1995/ResearchKit) of the project and implemented some customized requirements, including "locate button" for location selector, incorrect answer format form item color and small logic adjustments.

By implementing these changes, I gradually gained an elementary understanding of ObjC code. Right now, I can read some ObjC APIs and docs and understand the answers on SO, which is really helpful to learn older frameworks.

Moreover, instead of using Git Submodule or CocoaPods, I used Carthage to manage dependencies, as it can greatly integrate GitHub repository with local projects. I'll describe the details in section below.

#### Questionnaire

issues #7, #11, #18, #21

As you can imagine, there are countless ways for making a questionnaire to gather user preferences and answers. If you want, you can even use a series of pop-ups to achieve it. The most common way of doing it is to create something like a paper form, where user can skim from top to bottom of each field, and use separate pages to divide the topics.

FitnessFinder tried with 3 different designs, and finally sticked with the current page-based, arrow-navigation scheme.

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/json-editor.png" title="Questionnaire scheme generator" width="320">
</div>

- Question serialization

In the first sprint, the questions are provided in a text file from our client. It contains some question titles, and corresponding choices or descriptors such as "scale with slider", "choose a location". At first, I just hard coded every question into the source code.

After the second sprint, clients suggested that questions can be updated remotely. Therefore, I created a JSON editor online to provide a standardized interface for them to create a questionnaire, as it is shown on the right. It is intuitive than providing questions in a text file, and the schema can update as a hotfix in the app.

- Answer extraction

Another big topic is how to record the answers to the questions. At first I designed another model to fit any kind of answer, and coded as a JSON file. After the second sprint, our clients decided that they currently do not need to gather the results, hence all results can just remain in the app. I use an array of result structs to pass them between View Controllers.

- Construct SQL query statements

At first, we did not use any database the fmdb framework for query and filtering. Instead, we used a .csv spreadsheet as "database", manually parse the entries in that plain-text file, and construct the database dynamically in the memory when app launches. This approach is clearly not optimal, and is not friendly for future maintainers as well as data providers - They cannot work in parallel to add data to the database! Also, at that time, I created functions to deal with query requests, which requires me to take every possible condition into count. Discussion with Prof. Ric revealed that we were literally re-inventing SQL from scratch, aka "re-inventing the wheels". Therefore, we moved to SQLite database and fmdb ORM later on.

However, parsing the answers and constructing SQL statements still require some glue code. To make things easier, I just make raw SQL strings from the answers, so that it is easier to add more questions or to extend query capability.

#### Carthage

issues #24, #29

At the beginning of the project, there are many dependency management tools to choose from - Swift Package Manager, CocoaPods, Carthage, manual, etc. Since it was not complicated to manage dependencies by just dragging in project files, I sticked with manual management at first. Also, I used 
Git Submodule to manage versions of dependencies.

When I forked the ResearchKit and started customization, things got complex. I have to pull the source code every time when I update that framework, and it is not handy to do so with Git Submodule. Furthermore, GitLab and GitHub are not cooperating well when submodules are interleaved. Therefore, I moved to Carthage, as it supports updating from GitHub repo with simple commands, and nearly has the same behavior as manual management.

### Django back-end and fmdb

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/dbms.png" title="Django architecture" width="320">
</div>

issues #11, #18, #20, #22

The primary goal for creating a Django back-end service, is to utilize its robust user/group permission control, authentication, ORM and visualized database management interface. As I mentioned above, at first we use .csv file and JSON file to manage data entries, which works but is hardly maintainable. After several discussion and tests with Yuanyuan, we finally decided on Django as back-end, fmdb for client side and SQLite database. (Other options include Flask, Realm, PostgreSQL, MongoDB, CoreData, LAMP, etc.)

We created a back-end service based on Django framework, which provides 2 REST-like APIs: database MD5 checksum and download database.

#### DBMS

Based on Django Admin plug-in, a back-end GUI DBMS is set up. It has multiple features, includes but not limited to:

- Create an activity entry and store in the database
- Basic database CRUD operations with GUI
- Multi-user capability
- Group permission control: can allocate entry maintainers with no update/delete permissions
- Field validation with django-phonenumber-field package and validation rules
- Database checksum, which indicates either the client database need to be updated
- Database download, as the synchronization mechanism

For security reasons, the DBMS address will remain secret until the service is taken over by future maintainers.

#### Consistency check

When synchronizing database from back-end, the client will first checksum the local database replica to see if an update is needed. If the .sqlite3 file has the same MD5 checksum, then synchronization can be omitted since it is already the latest version. Otherwise, a download will be performed.

```swift
if remoteMD5 != localMD5 {
    self.downloadDB(url: ffdbURL)
}
else {
    dbInstance = FMDatabase(url: localDBPath)
    self.displayButtonsWhenSucceed()
}
```

#### Database synchronization

`Realm` database has nice synchronization support with `Ice Cream` package. MySQL has binlog which can be used to incrementally update local database. But these are not practical for SQLite db. The reason why we chose SQLite is its simplicity and compactness. Comparing to common heavier database instance running on a server, SQLite db is portable with just 1 file. After a few test runs, we decide to synchronize the whole file, as it would take less than 1MB of data transfer even when the entries count grows to ~300 rows. Right now our clients proposed a scale of 200 entries, which is manageable with this design.

By setting the response mimeType = "application/x-sqlite3" and do checksum at both ends, we can ensure the data integrity as well as its effectiveness. Also, to ensure each download request will be processed, following settings for the session need to be made.

```swift
let config = URLSessionConfiguration.default
config.requestCachePolicy = .reloadIgnoringLocalCacheData
config.urlCache = nil
```

### Workflow

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/flow.png" title="Question workflow, borrowed from ResearchKit" width="450">
</div>

The basic work flow for the app, as it is shown above in the videos in usage section, is to let users fill in a questionnaire and filter the results. Depend on clients requests, a to-do list and activity logging feature might be added in the future.

#### "The less taps, the better" 

Initially, we put all the criteria in the questionnaire. This resulted in a 10-page long survey, which requires more than a minute to complete. Our clients later suggested that the survey should be as short as possible, and reach the results before people lose focus.

Therefore, the current workflow will just require as few as 7 taps to reach the results table. Other criteria are put into the filter page for later use.

#### Reuse of ResearchKit

There are many fields that require user information in this app. In order to maintain a unified design, I managed to use ResearchKit to implement all of them. This will presumably ease the logics for users.

### Questions and filters

One issue for the workflow is how to balance between questionnaire and filter. The results can be too restricted if more questions are added to the questionnaire steps, while it will require more time to query the database if more criteria added to filters. Furthermore, decoupling and remove overlapped filters are important.

In order to mitigate those problems, I used set operations in the app. Due to the nature of mathematical sets, it is easy to calculate unions, intersections and complements of multiple criteria. Also, I enumerate the cases for different filters to get the correct chaining conditions for the results.

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/Map.png" title="Map" width="320">
</div>

### Location

issues #4, #32, #50, #51

One important function of the app is to filter away entries with geo-location. It is also a main reason that we want to build a native app instead of using existing web services such as Qualtrics.

The logic for location is described as this: the user will first pick their home location and their preferred distance to travel. Then the app will loop through all the activity entries, and decide if one is inside their preferred distance. Therefore, the map/location feature should provide several controls.

For question step:

- Locate me button, if the user is at home when using the app
- A fuzzy search text box where user can type in any address they want to start from
- A validation rule that validate the address in first place

For results:

- A map with a circumference that represents the preferred distance
- Pins for locations/activities
- For one location with multiple activities, such as a fitness center, it should only display one pin

In order to get locations within a certain distance, firstly the app converts location strings back to coordinates with Apple geo-location APIs. It has an asynchronous signature:

```swift
func geocodeAddressString(_ addressString: String, completionHandler: @escaping CLGeocodeCompletionHandler) {

}
```

Therefore, I need to set a barrier when all the entries get parsed, and execute the following logics with UI and table results. Details will be discussed in section below.

Another idea is to send back the coordinates and store them in the database. This is not implemented due to potential collisions. A better solution is yet to be found.

### Delegate, callback and asynchronous programming

Many time-consuming HTTP requests are involved in the app, including image download, geo-location reverse decode, database synchronization, questionnaire answer cleansing and UI update. Therefore, asynchronous programming patterns are widely used in the app.

- For lightweight async functions, the `completionHandler` method suggested by Apple is used. When the async call finishes, it will call the completionHandler closure to execute following code, pass through the results as parameters without blocking the main queue.

- For result-collecting functions, delegates are used for its clearness and effective memory management. For example, when collecting results from filter page, a delegate method is called.

```swift
func taskViewController(_ taskViewController: ORKTaskViewController, didFinishWith reason: ORKTaskViewControllerFinishReason, error: Error?) {
    if reason == .completed {
        // collect results here
    }    
}
```

- Callbacks are also used to learn about closures in Swift. At the time I implemented those features, I don't know about ARC yet, thus some of the callback closures cause retain cycle. Will be fixed in the future. ;-)

Also, at that time I don't quite understand the difference between instance methods and class methods. Anyway, below is an example for async calls in the location query class.

```swift
    /// get user's CLLocation coordinates from string
    ///
    /// - Parameters:
    ///   - address: String, the location string from user input. typically self.location if not nil
    ///   - completionHandler: after the geo-code finishes, execute further operations and pass user's coordinates into that closure
    func getLocation(address: String, completionHandler: @escaping (_ location: CLLocation) -> ()) {
        var myCoord: CLLocation!
        
        self.geoCoder.geocodeAddressString(address) { (pls: [CLPlacemark]?, error: Error?)  in
            if error == nil {
                let firstPL = pls?.first
                myCoord = firstPL?.location
            }
            else {
                print("Error: get coord for my location failed")
                // place user's chosen location to the Atlantic ocean, hence no results will come up.
                myCoord = CLLocation(latitude: CLLocationDegrees(exactly: 0)!, longitude: CLLocationDegrees(exactly: 0)!)
            }
            completionHandler(myCoord)  // call Swift completionHandler closure after retrieving coordinates.
        }
    }
```

### Miscellaneous

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/screenshot-share.jpg" title="Share with screenshot" width="320">
</div>

- Sharing with long screen-shot

In the first place, I came up with an idea, to share an activity with a screen-shot. It looks as the one on the right. This method requires scrolling the scrollView to certain offset, take screen-shots, and then stitch them together by calculating the offsets. While it works, it will take longer than simply sharing describing text, and need to take over the control of the device for several seconds, disabling user interaction to avoid faults in offset calculation. Therefore, this method was discarded.

- Reachability check

When the app launches, it will first run a network reachability check.

The logic of fetching remote DB:

- No connection:
    - Local non-exist, fallback to database file in bundle. Only happens at first launch with no connection
    - Local exists, skip MD5 and resume the app
- Has connection:  
    - Local non-exist, fetchAPI and skip MD5 and resume the app
    - Local exists, compare MD5
        - different, update
        - same, don't update and resume the app

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/FitnessFinder/screenshots/update-notice.png" title="Update Notice" width="200">
</div>

- Background & animation handling

Animations will stop when the app enters the background. Therefore, I set up observers in NotificationCenter to receive `didEnterBackgroundNotification` and `willEnterForegroundNotification`. When the app resumes, the animation will start again.

- Update notice

issue [#14](https://gitlab.oit.duke.edu/MobileCenter/task_queue/issues/14)

For all the apps delivered by Center of Mobile Development, I added a upgrade notice pop-up to them. Whenever a new version comes out, developers can easily set the upgrade message in a JSON file, and users will received the notice whenever they launch the app.

- Device orientation and layout

To make this project more complete, I managed to adapt the UI to all available iPhone and iPads dimensions. For layouts, I primarily used different .nib files to do so.

Also, for some of the pages, I override the function to handle device orientation change manually. Below is an example.

```swift
override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    coordinator.animate(alongsideTransition: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
        initFrameForButton(width: size.width, height: size.height, button: self.startOverButton)
    }, completion: nil)
}
```

### Group members and work distribution

While Yuanyuan was in hunt of full-time position at that time, she still contributed considerably to the project.

- Ting 80% – Organizer, main model designer, implement key features including client UI/UX, database interaction and network stuff.
- Yuanyuan 20% - Mainly dived into the background research of DBMS and voted for Django and fmdb as the core data management components.

## Progress notes

*Duke netid login required for links below.*

1. Please refer to the [issues](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/issues?scope=all&state=all) of FitnessFinder app to see why *Rome wasn't built in a day* - «Rome ne s'est pas faite en un jour», and how this project evolved.

2. Please refer to [👉this document👈](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/tree/master/progress-notes/progress-notes.md) to see the initial issues of the app.

3. If you like, check out the demo videos in each sprint.

## Contributing

The app can be downloaded at [Duke AppStore](https://appstore.colab.duke.edu/apps/77).

Contributions are very welcome 🙌.

Fixing any [issues](https://gitlab.oit.duke.edu/MobileCenter/fitnessfinder/issues?scope=all&state=all) in the list can be a good start!

## Credits

The template for this description is taken from [SwiftKit](https://github.com/SvenTiigi/SwiftKit/blob/master/Template/README.md).

Countless StackOverflow questions were referred to during the development. Appreciation is beyond my words.

- [ResearchKit](http://researchkit.org/index.html) for questionnaire

- [Carbon](https://github.com/ra1028/Carbon) for detail page form generator

- [Eureka](https://github.com/xmartlabs/Eureka) for form generator

- [KUIPopOver](https://github.com/Kofktu/KUIPopOver) for pop-up on iPhone

- [Lightbox](https://github.com/hyperoslo/Lightbox) for image carousel

## Heritage

The Sprint 0 version is [here](https://gitlab.oit.duke.edu/rt113/fitness_finder). Really primitive. Once I thought I might never deliver the app.

## License

```
FitnessFinder app
Copyright © Duke's Center for Mobile Development 2019
All rights reserved. 
```

#### last revision: 190722 - Author - Ting Chen
