> Please note: the original app description is at: https://gitlab.oit.duke.edu/MobileCenter/25live. Since the app is undisclosed under *Duke's Center for Mobile Development*, you need to login to Duke GitLab service before accessing its dependencies.

---

<div align="center">
   <img width="256" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/logo.png" alt="PrattQuickMeet Logo">
</div>

<div align="center">
    ![Swift](https://img.shields.io/badge/Swift-5.1-orange.svg?style=flat)
    ![Xcode](https://img.shields.io/badge/Xcode-11.0-blue.svg?style=flat)
    ![macOS](https://img.shields.io/badge/macOS-10.14.5-green.svg?style=flat)
    ![version](https://img.shields.io/badge/version-1.0.1%20build%206-blue.svg?style=flat)
</div>

<div align="center">
    <h1>PrattQuickMeet app - quickly find a meeting place around Pratt.</h1>
</div>

[PrattQuickMeet](https://appstore.colab.duke.edu/apps/79) helps people to quickly find a meeting room around *Pratt School of Engineering*, without diving into school event schedules and room allocation status. Just get a room instantly!

The app is developed by Haohong Zhao, Xin Rong and Ting Chen in *Duke's Center for Mobile Development* (CMD). Currently the app reaches beta release, and is being tested and used by around 10 users in *Duke Pratt School of Engineering*.

## Features

PrattQuickMeet app has a few main features:

- [x] Fetch, parse and display the room availability data from 25live.com
- [x] Filter meeting rooms with room type, capacity, location and key word
- [x] Categorize rooms by their expected availability
- [x] Reserve a room directly from the app with 25live embedded in
- [x] First app in CMD to fully work with iOS 13 beta Dark Mode!
- [x] ...More to be discovered in the app.

## Example

<div align="center">
    <img src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/videos/demo.gif" alt="Pick a random activity">
</div>

Above is a simple sample of PrattQuickMeet.

## Usage

**Please note:** videos on GitLab seems not to be able to play on mobile devices.  
Please visit the page with desktop browser to have a better experience.

| `Filter Demo` | 
|:---------------:|
|![Filter](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/videos/usage.mov)|
|PrattQuickMeet provides basic filtering features for meeting rooms. By providing basic information of the room, the user can easily spot the rooms that fit his/hers need.|

Videos are compressed with `FFmpeg`.
```bash
ffmpeg -i <input>.mov -vf scale=640:-1 <output>.mov
```

## Designs and highlights

| `welcome` | `filter` | `refresh` | `25live` |
|:---------------:|:---------------:|:---------------:|:---------------:|
|![welcome](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/screenshots/welcome.png)|![filter](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/screenshots/filter.png)|![refresh](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/screenshots/refresh.png)|![25live](https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/screenshots/25live.png)|

- Welcome page

    By embedding in the [WhatsNewKit](https://github.com/SvenTiigi/WhatsNewKit), user can receive the latest version welcome screen and instructions when they first launch the app after updating or installation. 

<div>
    <img style="float: right" align="right" src="https://gitlab.oit.duke.edu/tc233/common_host/raw/master/PQM/screenshots/dark.png" title="Dark Mode" width="320">
</div>

- Pull to refresh
    
    User can simply refresh the list by pulling down from the top of the table. The app will then fetch the newest room schedules from 25live.com

- All screen sizes and orientation adaptive
    
    The app is fully adapted to all screen sizes for both iPhone and iPad, and both horizontal and vertical orientations with respectively designed .nib files.

- Filters

    The app have a simple and neat filter pop-up that allow users to find a room's availability as quick as a flash. By applying multiple filter rules, users can easily lock up potential candidates.

- 25Live integration

    By entering a room's detail page, you can reserve the room on 25live.com from the app with great ease. The app will auto-fill expected fields such as room id, appointed time period and user identity with JavaScript. Due to current workflow requires examination of appointments by human, this feature is not enabled in beta version

- Dark Mode

    This is one of the very first few apps in CMD that is adapted to iOS 13 Dark Mode.

- Testing and edge case handling

    Since the app interact a lot with schedule and time, test cases were developed to test against various situation of overlapping reserved periods. Also, the app added special handling for the end of the day.

    Since the original JSON source is not well formated, the app also provide multiple fallbacks for unknown fields and values.

## Progress notes

*Duke netid login required for links below.*

Please refer to the [issues](https://gitlab.oit.duke.edu/MobileCenter/25live/issues?scope=all&state=all) of PrattQuickMeet app to see the progress of the app.

## Contributing

The app can be downloaded at [Duke AppStore](https://appstore.colab.duke.edu/apps/79).

Contributions are very welcome 🙌.

Fixing any [issues](https://gitlab.oit.duke.edu/MobileCenter/25live/issues?scope=all&state=all) in the list can be a good start!

## Credits

- The template for this description is taken from [SwiftKit](https://github.com/SvenTiigi/SwiftKit/blob/master/Template/README.md).

- [Eureka](https://github.com/xmartlabs/Eureka) for form generator

- [KUIPopOver](https://github.com/Kofktu/KUIPopOver) for pop-up on iPhone

- [Lightbox](https://github.com/hyperoslo/Lightbox) for image carousel

## License

```
PrattQuickMeet app
Copyright © Center for Mobile Development 2019
All rights reserved. 
```

#### last revision: 190721

---

## Additional notes

### For release

```sh
$ xcodebuild -workspace 25Live-MobileDevelopmentCenter.xcworkspace -scheme DukeAppStore clean build CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO
```

### For cleaning

```sh
$ xcodebuild clean

$ xcodebuild -workspace 25Live-MobileDevelopmentCenter.xcworkspace -scheme DukeAppStore clean 
```

### Open a path

<kbd>cmd</kbd> <kbd>shift</kbd> <kbd>G</kbd> to go to a directory from finder
