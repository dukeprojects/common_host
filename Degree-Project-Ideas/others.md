
1. Geo-fence duke map

To create an app that serves as an AR + audio tour guide of Duke campus. Staff will provide corresponding resources and materials, while I'll deal with geo-location recognizing with mobile devices, and AR integration that display images, text, etc.

pros:
- real need from Prof. Ric
- can be complex if want to elaborate
- learn about Geo things

cons:
- not really useful, only limited to people who come to campus visit
- many UI stuff need to be dealt with, other than architecture and algorithm
- not really complex
- more time to deal with client needs, (e.g. negotiate, define needs) than really writing code


2. other previous course projects

Can elaborate on one of those project that I've done during my graduate period. See in documentations below.

[Documentations](http://people.duke.edu/~tc233/xilinx-repo/)

- Omnimoji app

    Pros:

    - Many interesting features
    - Client-Server architecture
    - Fairly complex

    Cons:

    - Most of my desired features are done, remaining problems are largely UI refatorization
    - architecture is not even as complex as ECE564 server, just a simple HTTP server to deal with image processing and storage
    - Prof. Drew is doubtful of it

- NLP project

    Pros:

    - Academic, fairly complex
    - Can add more method to analyze lyrics

    Cons:
    - Results are not promising with traditional statistic methods, not meaningful
    - I don't want to use previous course projects of which I did not even get an A

- Image processing project

    Pros:
    - I'm interested in it, and want to re-investigate
    - Potential discovery might be there!

    Cons:
    - Results are trivial, not really meaningful
    - Workload is small


3. All the apps I've done

Check out at: https://appstore.colab.duke.edu/

Most of them are adequate in workload, but not complex in architecture. Plus, most of the released ones are done.
