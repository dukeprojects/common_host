## Final degree project ideas

My preferences are as rankings descending. Please view the links below on desktop browser.

1. iPod SfM - Quick 3D-Model generator + AR Application

    [details](/Degree-Project-Ideas/ipodsfm.md)

2. iPodClustr - Batch display

    [details](/Degree-Project-Ideas/ipodclustr.md)

3. ECE564 course design - WebServer + app

    [details](/Degree-Project-Ideas/DukePersonServer.md)

4. CJK categorizing/ResearchGate avatar analysis

    [details](/Degree-Project-Ideas/CJK.md)

5. Improvements of other previous coursework or small personal projects

    [details](/Degree-Project-Ideas/others.md)

---

I respect this valuable chance, as it might be my last ever pure interest-driven, non-profitable project.

Therefore, I want it to be concise in idea, complex and clear in structure and requires considerable amount of work to investigate and implement. The result should be meaningful and I shall be able to learn new skills from it.
