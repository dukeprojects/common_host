# iPodSfM

**iPodSfM** is a potential choice for my degree project. The goal is to embed the power of SfM algorithm and iOS ARKit to create a workflow that produces 3D model of an object with daily devices at minimum cost.

Please visit this page: http://people.duke.edu/~tc233/hosted_files/doc3.html with an iOS device to get a first look of the potential result. Tap on the helmet image in any browser, or the AR logo at upper right if you are using Safari.

## Idea

<div>
    <img style="float: right" align="right" src="/Degree-Project-Ideas/images/model-camera.png" title="Doraemon model camera" width="360">
</div>

- This comes from a random thought. When I was taking a 360° panorama photo, I thought: what if the camera aims at the radial direction instead of its opposite?

- People have been dreaming about making 3D replica of an object merely with camera and 3D printer for decades. When this idea came across me, I thought: why not try to implement a validating prototype with all the stuff I have?

- In 2019 Spring I took the **ECE590 - Digital Image Processing** class, and when we were learning about triangulation, I heard about an emerging algorithm in CV and DIP called [SfM](https://en.wikipedia.org/wiki/Structure_from_motion) from Patrick. [Some research groups](http://www.cs.cornell.edu/projects/bigsfm/) have been doing that for couple of years, and the reconstruction results are great.

- I did some researches online and there are no such lightweight applications of this algorithm, despite there are a few industrial setups which requires a room with hundreds of cameras. In terms of the algorithm, [Autodesk ReCap 360](https://www.autodesk.com/products/recap/overview) do the job quite well, but it is a closed source/proprietary software and subscription is expensive (Duke has education subscription though).

- Hence I want to make such a workflow that captures images from a ring of iPods, feed images to the server and run the algorithm, send back the generated model to device and display it with ARKit on iOS device. This would be great to create a 3D model for any smaller objects, and preview it in real environment with AR.

- Potential use cases include remote prototype sharing and more. For example, when I got a new gadget, and want to share its detailed look with my friends off-site, I can just make a 3D model and send the link to him, and let him see all aspects of it, instead of sending a deck of images or a go-around video.

- In short, it is a quick 3D model generator + AR Application

## Architecture and System design

![Workflow](/Degree-Project-Ideas/images/sfm-workflow.png)

Above demonstrates the projected workflow of this project.

### Problems to solve

To create a workflow that takes in images and return a 3D model which could be rendered by ARKit.

### Needed parts

#### hardware
 
- Multi-iPod with 3D printed skeleton image capture device

<div>
    <img style="float: right" align="right" src="/Degree-Project-Ideas/images/round.png" title="Batch iPods" width="360">
    <p>Adapted from Shot-on-iPhone ads</p>
</div>

Design a frame to attach 6 or 8 iPods on a circle, with all cameras aiming at the center.

- Server with CUDA environment

- iPad with AR capability

#### software

- Collect images from all devices and upload to server simultaneously

The server will wait for a certain number of images from the round cameras. When all count of images collected, the server will send a notification to the master device.

- Feed all images into the program

- ✅[Potential improvements] Camera calibration

since iPods are fixed aligned, maybe do some optimization

- [SIFT](https://en.wikipedia.org/wiki/Scale-invariant_feature_transform)  
feature extraction and image matching with SIFT algorithm

- ✅[Potential improvements] Photogrammetry with SfM

<div>
    <img style="float: right" align="right" src="/Degree-Project-Ideas/images/iPod_images.png" title="images" width="360">
    <p>Photos of a helmet taken with an iPhone</p>
</div>

Might either insert a pre-built open-source library or try to make some changes to the existing tools.

- ✅[Potential improvements] Depth map generation  
triangulation

- Meshing  
Combining depth map parts to form the mesh. This part is beyond my capability and there are certain tools to do this already.

- Texturing  

![Texture, Generated with ReCap 360](/Degree-Project-Ideas/videos/helmet_model.mov)

Generating texture map from photos for a 3D model. Beyond the scope, will use open source tools.

- Crop model with predefined dimensions

Generated model might contain detached edges and vertices. Crop the model according to the radius of device circle.

- Export the model

![AR app](/Degree-Project-Ideas/videos/helmet_ar.mp4)  
Above is a simple [AR app](https://github.com/yo1995/Daily_Swift_Tasks/tree/master/ARKit_101/ARKit_101) I wrote to verify the idea

Download the model to device and write an app to render it in AR.

## Highlights

- Improved SfM for certain use case

- Complex environment setup and configuration

- ARKit integration

## Pros and Cons

### pros
- really interesting; no existing mobile app do this
- combine signal processing and mobile development - two topics I'm interested in during my graduate period
- fairly complex; can control its complexity by opting in/out steps and parts
- passionate for the idea

### cons
- reuse lots of libraries, especially for 3D model meshing and SIFT algorithm
- can only generate static models
- resolution is limited in real practice, not as good as laser scan or MRI
- computation resources limited  
have to learn about CUDA deployment and might need to take advantage of AWS cloud. otherwise it will take too long to generate a model. my test render of the above helmet model took around 50 mins on my laptop without CUDA, and it took around (9 minutes from upload to download, 6.5 minutes for rendering， total with 20 images) with ReCap 360.

The result of above test is as below.

![Test](/Degree-Project-Ideas/videos/20T.mov)  


## Roadmap

1. Look into AWS and environment settings, try to run existing open-source software
2. Test to see if the whole process can finish within 5 minutes
3. start to prepare the hardware
4. During the semester, create the mobile app, as well as to modify the image matching part and SfM part of server side programs
5. Test the device and workflow with real objects, figure out limitations and potential improvements
6. Revise and improve before graduation
7. Sum up as the degree project

---

![AR with SketchFab](/Degree-Project-Ideas/videos/pokemon_ar.mov)

