# Face feature extracting - CJK

The goal of this idea is to analysis some features from different datasets of human faces.

## Idea

- From time to time, when I see an Asian face, I ask myself: is that guy Chinese, Japanese or Korean?

	[All look same?](http://alllooksame.com/)
	[Asian](https://www.washingtonpost.com/news/wonk/wp/2016/10/21/the-awkward-thing-that-happened-when-scientists-asked-a-computer-to-tell-asian-faces-apart/?noredirect=on&utm_term=.ed9839844af0)

- During my undergrad, when I was using ResearchGate, I saw avatars of scholars from all over the world. Many looks knowledgeable, and quite a few wear glasses. At that time I wonder: do those scholars have different facial features than normal people?

## Problems to solve

I'd like to either reproduce the result and improve based on the [paper](https://arxiv.org/pdf/1610.01854v1.pdf), or do some similar research for ResearchGate avatars.

1. gather datasets.

Currently I know famous face datasets such as [CelebA](http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html), [CASIA-FaceV5](http://biometrics.idealtest.org/dbDetailForUser.do?id=9), [IMDB-WIKI](https://data.vision.ee.ethz.ch/cvl/rrothe/imdb-wiki/) and so on.

To acquire the avatars from ResearchGate, I plan to write a spider and run a scheduled task from multiple servers. Avatars will then be tagged with location, research interest and other information.

2. extract features from images

3. evaluate results

## Pros and Cons

### pros
- The project can answer my own curiosity
- It is fairly complex - gather face dataset with writing a scraper or spider, find and differentiate tagged datasets from Internet, or collaborate with researchers to learn about their work
- Might be useful as an API after the model is trained. I can take a picture and use API to make a prediction, better than my pure guessing
- Sort of academic
- I want to learn more about neural network as an additional skill

### cons

- I don't have deep learning background and might only be able to do feature engineering. Not really into the core of the topic. 
- Will use existing tools
- Not aligned with my career path, just as a hobby-ish project
- Legal and ethic issues
- Analyze a problem is not as good as purpose a new method to tackle the problem

---

